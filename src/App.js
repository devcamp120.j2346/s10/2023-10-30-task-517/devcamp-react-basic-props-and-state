import CountClick from "./components/CountClick";
import Info from "./components/Info";

function App() {
  return (
    <div style={{padding: "20px"}}>
      <Info firstName="Minh" lastName="Vu Dang" favNumber={24}></Info>

      <CountClick />
    </div>
  );
}

export default App;

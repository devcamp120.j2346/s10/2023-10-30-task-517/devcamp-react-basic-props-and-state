import { Component } from "react";

class Info extends Component {
    render() {
        console.log(this.props);

        var { firstName, lastName, favNumber } = this.props;

        return (
            <div>
                <p>My name is {lastName} {firstName} and my favourite number is {favNumber}.</p>
            </div>
        )
    }
}

export default Info;
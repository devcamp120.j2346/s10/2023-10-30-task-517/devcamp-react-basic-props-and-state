import { Component } from "react";

class CountClick extends Component {
    constructor(props) {
        super(props);

        this.state = {
            count: 0
        }

        // this.onButtonClicked = this.onButtonClicked.bind(this);
    }

    onButtonClicked = () => {
        console.log("Nút được bấm");

        this.setState({
            count: this.state.count + 1
        })
    }

    render() {
        return (
            <div>
                <div>
                    <button onClick={this.onButtonClicked}>Click here!</button>
                </div>
                <div>
                    <p>You clicked {this.state.count} times</p>
                </div>
            </div>
        )
    }
}

export default CountClick;